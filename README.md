# NssMPClib

## 介绍

本项目为安全多方计算库，设计并实现了基于算术秘密分享，函数秘密分享的隐私保护计算协议，并基于这些协议实现隐私保护机器学习应用：隐私保护神经网络推理。

## 安装教程

本项目需要Pytorch>=1.8.0支持，建议使用Pytorch==2.3.0

项目本体使用以下指令即可安装

```bash
pip install .
```

注意若无法编译安装依赖csprng，可能是因为缺少c++编译器或cuda toolkit

## 使用说明

关于如何使用该计算库进行隐私保护应用，请参考```tutorials```包中的教程，这些内容以 Jupyter 笔记本的形式呈现，因此请在您的
conda 环境中安装以下内容

```bash
conda install ipython jupyter
```

1. `Tutorial_0_Before_Starting.ipynb` - 在教程开始前，对该计算库内的配置信息和计算所需的辅助参数做一定介绍
2. `Tutorial_1_Ring_Tensor.ipynb` - 介绍了该计算库的基本数据类型：`RingTensor`，并展示了如何使用`RingTensor`进行基础运算
3. `Tutorial_2_Arithmetic_Secret_Sharing.ipynb` - 介绍了该计算库中用于安全多方计算的基本数据类型:
   `ArithmeticSecretSharing`
   ，利用算术秘密分享技术将数据分享成两个份额，分发给两个参与方。该教程中还演示了如何利用`ArithmeticSecretSharing`进行基础运算
4. `Tutorial_3_Replicated_Secret_sharing.ipynb` - 介绍了该计算库中用于安全多方计算的基本数据类型:
   `ReplicatedSecretSharing`
   ，利用复制秘密分享技术将数据分享成多个份额，分发给多个参与方。`ReplicatedSecretSharing`
   的基础运算与`ArithmeticSecretSharing`类似，相关基础运算教程敬请期待。
5. `Tutorial_4_Generate_Beaver_Triples_by_HE.ipynb` - 介绍了如何利用同态加密生成Beaver三元组
6. `Tutorial_5_Parameter.ipynb` - 介绍如何在该计算库的基础上设计，实现，生成和使用安全多方计算所需的辅助参数
7. `Tutorial_6_Function_Secret_Sharing.ipynb` - 该计算库中函数秘密分享教程，介绍分布式点函数，分布式比较函数，分布式区间函数生成其密钥和评估的过程
8. `Tutorial_7_Neural_Network_Inference.ipynb` - 介绍如何利用该计算库的接口实现隐私保护神经网络推理

## 项目架构

- **csprng**  
  定制化后的torchcsprng源码，作为计算库的外部依赖
- **data**  
  用于存放隐私保护神经网络推理的明文模型结构代码，其他应用所需的相关数据也可放在该文件夹下
- **debug**  
  本项目的测试代码
- **NssMPC**  
  计算库主体源码
    - application  
      application包中是基于计算库的基础功能实现的应用，目前包括隐私保护的神经网络推理，支持明密文模型的自动转换与神经网络密态推理
    - common  
      common包中包括该计算库用到的通用工具和基础数据结构，包括网络通信，随机数生成器和其他工具
    - config  
      config包中包括该计算库的基础配置和网络配置
    - crypto  
      crypto是该库的核心部分，包括该库支持的隐私计算密码学原语和协议
    - secure_model  
      secure_model包中包括该库用到的系统模型和威胁模型及相关工具，如半诚实假设下的客户端-服务器模型
- **tutorials**  
  本项目的教程文档

## API文档

详细的接口文档见[NssMPClib Documentation](https://www.xidiannss.com/doc/NssMPClib/index.html)

## 维护者

本项目由[西安电子科技大学NSS实验室数据安全组](https://www.xidiannss.com/)负责维护。

## 使用许可

本项目使用MIT开源协议，如在LICENSE中所述。

